package app.android.neo.view

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import app.android.neo.R
import app.android.neo.model.data.Employee
import app.android.neo.model.data.Item
import app.android.neo.util.ClickListener
import app.android.neo.util.RecyclerTouchListener
import app.android.neo.view.activity.LoginActivity
import app.android.neo.view.adapter.NavigationRVAdapter
import app.android.neo.view.fragment.EmployeesFragment
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.yarolegovich.slidingrootnav.SlidingRootNav
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.drawer_menu.*
import java.io.File
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private var slidingRootNav: SlidingRootNav? = null
    private lateinit var adapter: NavigationRVAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadMenu(savedInstanceState)
        checkSesion()

        closeSe.setOnClickListener {
            val pref = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
            pref.clear()
            pref.apply()

            val intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        }
    }

    private fun loadMenu(savedInstanceState: Bundle?) {
        slidingRootNav = SlidingRootNavBuilder(this)
            .withToolbarMenuToggle(toolbar)
            .withMenuOpened(false)
            .withContentClickableWhenMenuOpened(false)
            .withSavedState(savedInstanceState)
            .withMenuLayout(R.layout.drawer_menu)
            .withDragDistance(100)
            .inject()

        list.layoutManager = LinearLayoutManager(applicationContext)
        list.setHasFixedSize(true)
        updateAdapter(0)
        onNavigated()
        loadFrgment(EmployeesFragment())
    }

    private fun loadFrgment(fragment: Fragment){
        supportFragmentManager.beginTransaction().replace(R.id.Container, fragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .commit()
    }

    override fun onBackPressed() {
        if (slidingRootNav!!.isMenuOpened){
            slidingRootNav!!.closeMenu()
        }else{
            super.onBackPressed()
        }
    }

    private fun loadItems() : List<Item>{
        val list = mutableListOf<Item>()

        list.add(
            Item(
                isSelected = true, name = "Employees", icon = ContextCompat
                    .getDrawable(
                        applicationContext,
                        R.drawable.ic_list
                    )!!
            )
        )
        list.add(
            Item(
                isSelected = false, name = "Setting", icon = ContextCompat
                    .getDrawable(
                        applicationContext,
                        R.drawable.ic_settigs
                    )!!
            )
        )

        return list
    }

    private fun onNavigated(){
        list.addOnItemTouchListener(
            RecyclerTouchListener(this,
                object : ClickListener {
                    override fun onClick(view: View, position: Int) {
                        when (position) {
                            0 -> {
                                loadFrgment(EmployeesFragment())
                                Handler(Looper.getMainLooper()).postDelayed(
                                    {
                                        slidingRootNav!!.closeMenu()
                                    }, 200
                                )
                            }
                            1 -> {
                                selectImage()
                            }
                        }

                        updateAdapter(position)
                    }
                }
            )
        )
    }

    private fun selectImage() {
        ImagePicker.create(this)
            .returnMode(ReturnMode.ALL)
            .single()
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val image = ImagePicker.getFirstImageOrNull(data)

            val pref = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
            pref.putString("image", image.path)
            pref.apply()

            try {
                imgProfile.setImageURI(Uri.fromFile(File(image.path)))
            } catch (e : Exception){
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun updateAdapter(highlightItemPos: Int) {
        adapter = NavigationRVAdapter(
            loadItems(),
            highlightItemPos
        )
        list.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun checkSesion() {
        val pref = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)
        val e = pref.getString("data", null)
        if (e != null){
            val a = Gson().fromJson<Employee>(e, object : TypeToken<Employee>() {}.type)

            if (a.employee_name.isNotEmpty()){
                //TODO data employee
                empName.text = a.employee_name
            }
            else {
                val intent = Intent(this, LoginActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                this.finish()
            }

        } else{
            val intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        }

        val image = pref.getString("image", null)

        if (image != null) {
            try {
                imgProfile.setImageURI(Uri.fromFile(File(image)))
            } catch (e : Exception){
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
            }
        }
    }
}