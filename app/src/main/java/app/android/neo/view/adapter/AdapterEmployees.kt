package app.android.neo.view.adapter

import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import app.android.neo.R
import app.android.neo.model.data.Employee
import kotlinx.android.synthetic.main.activity_data.*
import kotlinx.android.synthetic.main.item_emp.view.*

class AdapterEmployees : RecyclerView.Adapter<AdapterEmployees.EmployeeViewHolder>(){

    private var data: List<Employee> = ArrayList()
    lateinit var listener: EmployeeListener

    fun swapData(data: List<Employee>) {
        this.data = data
        notifyDataSetChanged()
    }

    class EmployeeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var listener: EmployeeListener? = null

        fun bind(item: Employee, listener: EmployeeListener, position: Int) = with(itemView) {

            this@EmployeeViewHolder.listener = listener
            var lastClickTime: Long = 0

            eId.text = item.id
            eName.text = item.employee_name
            eSalary.text = item.employee_salary

            if (item.employee_salary.toLong() > 150000.toLong()){
                eSalary.setTextColor(ContextCompat.getColor(itemView.context, R.color.green))
            } else{
                eSalary.setTextColor(ContextCompat.getColor(itemView.context, R.color.red))
            }

            eAge.text = item.employee_age

            setOnClickListener {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return@setOnClickListener;
                }
                lastClickTime = SystemClock.elapsedRealtime()

                this@EmployeeViewHolder.listener!!.onEmployeeSelected(position, item)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        return EmployeeViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_emp, parent, false)
        )
    }

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) =
        holder.bind(data[position], listener, position)

    override fun getItemCount(): Int = data.size
}

interface EmployeeListener {
    fun onEmployeeSelected(position: Int, e : Employee)
}