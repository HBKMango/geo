package app.android.neo.view.activity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import app.android.neo.R
import app.android.neo.model.data.Employee
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_data.*

class DataActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data)

        val bundle = intent.extras
        val data = bundle?.getString("data")

        if (data != null){
            val a = Gson().fromJson<Employee>(data, object : TypeToken<Employee>() {}.type)

            EId.text = a.id

            EName.text = a.employee_name

            EAge.text = a.employee_age
            if (a.employee_age.toInt() in 26..34){
                EAge.setTextColor(ContextCompat.getColor(applicationContext, R.color.green))
            } else{
                EAge.setTextColor(ContextCompat.getColor(applicationContext, R.color.red))
            }

            ESalary.text = "$ ${a.employee_salary}"
        }

    }
}