package app.android.neo.view.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.android.neo.R
import app.android.neo.model.data.Employee
import app.android.neo.presenter.PresenterEmploye
import app.android.neo.presenter.contract.ContractEmploye
import app.android.neo.view.activity.DataActivity
import app.android.neo.view.adapter.AdapterEmployees
import app.android.neo.view.adapter.EmployeeListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_employees.*

class EmployeesFragment : Fragment(), ContractEmploye.View, EmployeeListener {

    private lateinit var presenter : PresenterEmploye
    private lateinit var adapter : AdapterEmployees

    override fun onAttach(context: Context) {
        super.onAttach(context)

        presenter = PresenterEmploye(this)
        adapter = AdapterEmployees()
        adapter.listener = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.getEmployees()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_employees, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerE.adapter = adapter
        recyclerE.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
    }

    override fun onEmployeesGotten(data: List<Employee>) {
        adapter.swapData(data)
    }

    override fun onResponseFailure(throwable: String) {
        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
    }

    override fun showLottie() { }

    override fun hideLottie() { }

    override fun onEmployeeSelected(position: Int, e: Employee) {
        val jsonString = Gson().toJson(e)

        val intent = Intent(context, DataActivity::class.java).apply {
            putExtra("data", jsonString)
        }
        startActivity(intent)
    }

}