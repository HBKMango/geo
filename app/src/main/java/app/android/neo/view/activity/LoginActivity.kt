package app.android.neo.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.widget.Toast
import app.android.neo.R
import app.android.neo.model.data.Employee
import app.android.neo.model.data.ResponseEmployee
import app.android.neo.presenter.PresenterLogin
import app.android.neo.presenter.contract.ContractLogin
import app.android.neo.util.Lottie
import app.android.neo.view.MainActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), ContractLogin.View {

    private lateinit var presenter : PresenterLogin
    private lateinit var animation : Lottie
    private var lastClickTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        presenter = PresenterLogin(this)
        animation = Lottie()
    }

    override fun onStart() {
        super.onStart()

        btnLogin.setOnClickListener {

            if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                return@setOnClickListener;
            }
            lastClickTime = SystemClock.elapsedRealtime()

            if (inputID.text.isNotEmpty()){
                presenter.login(inputID.text.toString())
            } else{
                contentID.isErrorEnabled = true
            }
        }
    }

    override fun onLogin(data: ResponseEmployee) {
        saveNotes(data.data)

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onResponseFailure(throwable: String) {
        Toast.makeText(this@LoginActivity, throwable, Toast.LENGTH_SHORT).show()
    }

    override fun showLottie() {
        animation.show(supportFragmentManager, "Dialog")
    }

    override fun hideLottie() {
        animation.dismiss()
    }

    @SuppressLint("CommitPrefEdits")
    private fun saveNotes(e: Employee){
        val pref = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
        val jsonString = Gson().toJson(e)
        pref.putString("data", jsonString).apply()
    }
}