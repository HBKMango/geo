package app.android.neo.view.activity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import app.android.neo.R
import app.android.neo.model.data.Employee
import app.android.neo.view.MainActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


        Handler(Looper.getMainLooper()).postDelayed(
            {
            checkSesion()
        }, 3000)
    }

    private fun checkSesion() {
        val pref = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)
        val e = pref.getString("data", null)
        if (e != null){
            val a = Gson().fromJson<Employee>(e, object : TypeToken<Employee>(){}.type)

            if (a.employee_name.isNotEmpty()){
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            else {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
        } else{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }
}