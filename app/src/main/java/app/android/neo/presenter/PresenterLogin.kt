package app.android.neo.presenter

import app.android.neo.model.ModelLogin
import app.android.neo.model.data.ResponseEmployee
import app.android.neo.presenter.contract.ContractLogin

class PresenterLogin (private var view : ContractLogin.View?)
    : ContractLogin.Presenter, ContractLogin.Model.OnFinishedListener{

    private val model : ContractLogin.Model = ModelLogin()

    //TODO --------------------------------------Presenter------------------------------------------

    override fun onDestroy() {
        view = null
    }

    override fun login(data: String) {
        view?.showLottie()
        model.login(this, data)
    }

    //TODO ----------------------------------------Finish-------------------------------------------

    override fun onLogin(data: ResponseEmployee) {
        view?.onLogin(data)
        view?.hideLottie()
    }

    override fun onFailure(throwable: String) {
        view?.onResponseFailure(throwable)
        view?.hideLottie()
    }
}