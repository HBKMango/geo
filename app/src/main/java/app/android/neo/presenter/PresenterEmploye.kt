package app.android.neo.presenter

import app.android.neo.model.ModelEmployes
import app.android.neo.model.data.Employee
import app.android.neo.presenter.contract.ContractEmploye

class PresenterEmploye(private var view: ContractEmploye.View?) :
    ContractEmploye.Presenter, ContractEmploye.Model.OnFinishedListener {

    private var model: ContractEmploye.Model = ModelEmployes()

    //TODO --------------------------------------Presenter------------------------------------------

    override fun onDestroy() {
        view = null
    }

    override fun getEmployees() {
        model.getEmployees(this)
    }

    //TODO ----------------------------------------Finish-------------------------------------------

    override fun onEmployeesGotten(data: List<Employee>) {
        view?.onEmployeesGotten(data)
    }

    override fun onFailure(throwable: String) {
        view?.onResponseFailure(throwable)
    }

}