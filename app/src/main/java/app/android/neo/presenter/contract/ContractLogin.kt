package app.android.neo.presenter.contract

import app.android.neo.model.data.ResponseEmployee

interface ContractLogin {

    interface Model {
        interface OnFinishedListener {
            fun onLogin(data : ResponseEmployee)
            fun onFailure(throwable: String)
        }
        fun login(onFinishedListener: OnFinishedListener, data : String)
    }

    interface View {
        fun onLogin(data : ResponseEmployee)
        fun onResponseFailure(throwable: String)
        fun showLottie()
        fun hideLottie()
    }

    interface Presenter {
        fun onDestroy()
        fun login(data : String)
    }

}