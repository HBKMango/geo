package app.android.neo.presenter.contract

import app.android.neo.model.data.Employee

interface ContractEmploye {
    interface Model {
        interface OnFinishedListener {
            fun onEmployeesGotten(data : List<Employee>)
            fun onFailure(throwable: String)
        }
        fun getEmployees(onFinishedListener: OnFinishedListener)
    }

    interface View {
        fun onEmployeesGotten(data : List<Employee>)
        fun onResponseFailure(throwable: String)
        fun showLottie()
        fun hideLottie()
    }

    interface Presenter {
        fun onDestroy()
        fun getEmployees()
    }
}