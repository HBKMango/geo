package app.android.neo.model

import app.android.neo.model.api.Api
import app.android.neo.model.data.ResponseEmployee
import app.android.neo.presenter.contract.ContractLogin
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ModelLogin : ContractLogin.Model {

    override fun login(onFinishedListener: ContractLogin.Model.OnFinishedListener, data: String) {

        val apiData = Api()

        apiData.api.getEmployee(data).enqueue(object : Callback<ResponseEmployee>{
            override fun onResponse(
                call: Call<ResponseEmployee>,
                response: Response<ResponseEmployee>
            ) {
                if (response.isSuccessful){
                    if (response.body()?.data != null){
                        onFinishedListener.onLogin(response.body()!!)
                    }
                    else{
                        onFinishedListener.onFailure("User not found")
                    }
                }
            }

            override fun onFailure(call: Call<ResponseEmployee>, t: Throwable) {
                onFinishedListener.onFailure("Error in the service")
            }
        })
    }
}