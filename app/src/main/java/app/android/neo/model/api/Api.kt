package app.android.neo.model.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class Api {
    private val BASE_URL : String = "http://dummy.restapiexample.com/"
    private val URL : String = BASE_URL + "api/v1/"

    val api: ApiEndPoints

    init {
        val login = HttpLoggingInterceptor()
        login.level = HttpLoggingInterceptor.Level.BODY

        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.addInterceptor(login).callTimeout(30, TimeUnit.SECONDS)

        val client = clientBuilder.build()
        val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        api = retrofit.create(ApiEndPoints::class.java)
    }
}