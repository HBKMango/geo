package app.android.neo.model

import app.android.neo.model.api.Api
import app.android.neo.model.data.ResponseEmployees
import app.android.neo.presenter.contract.ContractEmploye
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ModelEmployes : ContractEmploye.Model {

    override fun getEmployees(onFinishedListener: ContractEmploye.Model.OnFinishedListener) {
        val apiData = Api()

        apiData.api.getEmployees().enqueue(object : Callback<ResponseEmployees>{
            override fun onResponse(
                call: Call<ResponseEmployees>,
                response: Response<ResponseEmployees>
            ) {
                if (response.isSuccessful){
                    if (response.body()!!.data.isNotEmpty()){
                        onFinishedListener.onEmployeesGotten(response.body()!!.data)
                    } else {
                        onFinishedListener.onFailure("Error in the service")
                    }
                } else{
                    onFinishedListener.onFailure("Error in the service")
                }

            }

            override fun onFailure(call: Call<ResponseEmployees>, t: Throwable) {
                onFinishedListener.onFailure("Error in the service")
            }
        })
    }
}