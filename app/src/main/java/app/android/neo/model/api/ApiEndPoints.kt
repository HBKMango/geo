package app.android.neo.model.api

import app.android.neo.model.data.Employee
import app.android.neo.model.data.ResponseEmployee
import app.android.neo.model.data.ResponseEmployees
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiEndPoints {

    @GET("employee/{user_id}")
    fun getEmployee(@Path(value = "user_id", encoded = true) id: String?) : Call<ResponseEmployee>

    @GET("employees")
    fun getEmployees() : Call<ResponseEmployees>

}