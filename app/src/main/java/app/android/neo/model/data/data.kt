package app.android.neo.model.data

import com.google.gson.annotations.SerializedName

data class Employee(
    val id : String,
    val employee_name : String,
    val employee_salary : String,
    val employee_age : String,
    val profile_image : String
)

data class ResponseEmployee(
    val status : String,
    val data : Employee,
    val message : String
)

data class ResponseEmployees(
    @SerializedName("status")
    val status : String,

    @SerializedName("data")
    val data : List<Employee>
)

