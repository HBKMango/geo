package app.android.neo.model.data

import android.graphics.drawable.Drawable

data class Item(
    var isSelected : Boolean,
    val name : String,
    val icon : Drawable
)